#pragma once

namespace Scatter {
class LevelManager;
}
#ifndef LEVELMANAGER_HPP
#define LEVELMANAGER_HPP
#ifndef NDEBUG
#define URHO3D_DEBUG_ASSERT
#endif

#include "easyscript/Namespace.hpp"
#include "easyscript/SceneManager.hpp"

#include <Urho3D/Engine/Application.h>
#include <Urho3D/Core/Variant.h>



namespace Game {
class Physics;

class LevelManager : public SceneManager {
    eastl::string level = "main";

public:
    Physics *physics;

    using SceneManager::SceneManager;

    void LoadPhysics();
    void LoadLevel();

    void Start() {
        Variant v;
        v.SetCustom<LevelManager*>(this);
        app->SetGlobalVar("LevelManager", v);
        LoadLevel();
    }
};
}
#endif // LEVELMANAGER_HPP
