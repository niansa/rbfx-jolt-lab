﻿#include "CollisionShape.hpp"
#include "RigidBody.hpp"

#include <algorithm>

#include <Jolt/Jolt.h>
#include <Jolt/Physics/Collision/Shape/BoxShape.h>
#include <Jolt/Physics/Collision/Shape/SphereShape.h>
#include <Jolt/Physics/Collision/Shape/CylinderShape.h>
#include <Jolt/Physics/Collision/Shape/StaticCompoundShape.h>
#include <Jolt/Physics/Body/Body.h>
#include <Jolt/Physics/Body/BodyCreationSettings.h>



namespace Game {
static const char* typeNames[] =
{
    "Box",
    "Cylinder",
    "Sphere",
    nullptr
};


struct CollisionShape::Internal {
    /// Jolt collision shape settings.
    ea::unique_ptr<JPH::ShapeSettings> shapeSettings_ = nullptr;
    /// Jolt collision shape.
    JPH::Ref<JPH::Shape> shape_;
};


CollisionShape::CollisionShape(Context* context) :
    Component(context),
    position_(Vector3::ZERO),
    rotation_(Quaternion::IDENTITY),
    size_(Vector3::ONE),
    internal_(new Internal)
{
}
CollisionShape::~CollisionShape()
{
    delete internal_;
}

void CollisionShape::RegisterObject(Context* context)
{
    context->AddFactoryReflection<CollisionShape>(Category_Physics);

    URHO3D_ACCESSOR_ATTRIBUTE("Is Enabled", IsEnabled, SetEnabled, bool, true, AM_DEFAULT);
    URHO3D_ENUM_ATTRIBUTE_EX("Shape Type", shapeType_, MarkShapeDirty, typeNames, SHAPE_BOX, AM_DEFAULT);
    URHO3D_ATTRIBUTE_EX("Size", Vector3, size_, MarkShapeDirty, Vector3::ONE, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Offset Position", GetPosition, SetPosition, Vector3, Vector3::ZERO, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Offset Rotation", GetRotation, SetRotation, Quaternion, Quaternion::IDENTITY, AM_DEFAULT);
}

void CollisionShape::ApplyAttributes()
{
    UpdateShape();
}

void CollisionShape::OnSetEnabled()
{
    UpdateRigidBody();
}

void CollisionShape::InternalSetBox(const Vector3& size)
{
    internal_->shapeSettings_ = ea::make_unique<JPH::BoxShapeSettings>(JPH::Vec3(size.x_, size.y_, size.z_));

    shapeType_ = SHAPE_BOX;

    UpdateRigidBody();
}
void CollisionShape::SetBox(const Vector3& size, const Vector3& position, const Quaternion& rotation)
{
    size_ = size;
    position_ = position;
    rotation_ = rotation;

    InternalSetBox(size);
}

void CollisionShape::InternalSetSphere(float radius)
{
    internal_->shapeSettings_ = ea::make_unique<JPH::SphereShapeSettings>(radius);

    shapeType_ = SHAPE_SPHERE;

    UpdateRigidBody();
}
void CollisionShape::SetSphere(float diameter, const Vector3& position, const Quaternion& rotation)
{
    const float radius = diameter * 0.5f;

    size_ = Vector3(radius, radius, radius);
    position_ = position;
    rotation_ = rotation;

    InternalSetSphere(radius);
}

void CollisionShape::InternalSetCylinder(float radius, float halfHeight)
{
    internal_->shapeSettings_ = ea::make_unique<JPH::CylinderShapeSettings>(halfHeight, radius);

    shapeType_ = SHAPE_CYLINDER;

    UpdateRigidBody();
}
void CollisionShape::SetCylinder(float diameter, float height, const Vector3& position, const Quaternion& rotation)
{
    const float halfHeight = height * 0.5f;
    const float radius = diameter * 0.5f;

    size_ = Vector3(radius, halfHeight, radius);
    position_ = position;
    rotation_ = rotation;

    InternalSetCylinder(radius, halfHeight);
}

void CollisionShape::SetShapeType(ShapeType type)
{
    if (type != shapeType_)
    {
        shapeType_ = type;
        UpdateShape();
        UpdateRigidBody();
    }
}

void CollisionShape::SetSize(const Vector3& size)
{
    if (size != size_)
    {
        size_ = size;
        UpdateShape();
        UpdateRigidBody();
    }
}

void CollisionShape::SetPosition(const Vector3& position)
{
    if (position != position_)
    {
        position_ = position;
        UpdateRigidBody();
    }
}

void CollisionShape::SetRotation(const Quaternion& rotation)
{
    if (rotation != rotation_)
    {
        rotation_ = rotation;
        UpdateRigidBody();
    }
}

void CollisionShape::SetTransform(const Vector3& position, const Quaternion& rotation)
{
    if (position != position_ || rotation != rotation_)
    {
        position_ = position;
        rotation_ = rotation;
        UpdateRigidBody();
    }
}

const JPH::ShapeSettings& CollisionShape::GetCollisionShapeSettings() const
{
    return *internal_->shapeSettings_;
}

BoundingBox CollisionShape::GetWorldBoundingBox() const
{
    /*if (internal_->shape_ && node_)
    {
        // Use the rigid body's world transform if possible, as it may be different from the rendering transform
        auto* body = GetComponent<RigidBody>();
        Matrix3x4 worldTransform = body ? Matrix3x4(body->GetPosition(), body->GetRotation(), node_->GetWorldScale()) :
            node_->GetWorldTransform();

        Vector3 worldPosition(worldTransform * position_);
        Quaternion worldRotation(worldTransform.Rotation() * rotation_);
        btTransform shapeWorldTransform(ToBtQuaternion(worldRotation), ToBtVector3(worldPosition));
        btVector3 aabbMin, aabbMax;
        shape_->getAabb(shapeWorldTransform, aabbMin, aabbMax);

        return BoundingBox(ToVector3(aabbMin), ToVector3(aabbMax));
    }
    else  TODO*/
        return BoundingBox();
}

void CollisionShape::UpdateRigidBody()
{
    auto body = GetComponent<RigidBody>();
    if (body)
        body->UpdateColliders();
}

JPH::Ref<JPH::Shape> CollisionShape::CreateShape() const {
    return internal_->shapeSettings_ ? internal_->shapeSettings_->Create().Get() : nullptr;
}

void CollisionShape::UpdateShape()
{
    URHO3D_PROFILE("UpdateCollisionShape");

    if (node_)
    {
        switch (shapeType_)
        {
        case SHAPE_BOX:
            InternalSetBox(size_);
            break;

        case SHAPE_SPHERE:
            InternalSetSphere(Max(size_.x_, Max(size_.y_, size_.z_)));
            break;

        case SHAPE_CYLINDER:
            InternalSetCylinder(Max(size_.x_, size_.z_), size_.y_);
            break;
        }
    }
}
}
