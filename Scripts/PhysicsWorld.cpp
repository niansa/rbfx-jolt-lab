#include "PhysicsWorld.hpp"
#include "RigidBody.hpp"
#include "../PhysicsWorldInternals.hpp"
#include "easyscript/Namespace.hpp"

#include <Urho3D/Core/Assert.h>


#include <Jolt/Jolt.h>
#include <Jolt/RegisterTypes.h>
#include <Jolt/Core/Factory.h>
#include <Jolt/Physics/PhysicsSettings.h>
#include <Jolt/Physics/PhysicsSystem.h>
#include <Jolt/Physics/Body/Body.h>
#include <Jolt/Physics/Body/BodyCreationSettings.h>



namespace Game {
namespace Jolt {
// Function that determines if two object layers can collide
static bool MyObjectCanCollide(JPH::ObjectLayer inObject1, JPH::ObjectLayer inObject2) {
    return inObject1 & inObject2;
};

// Function that determines if two broadphase layers can collide
static bool MyBroadPhaseCanCollide(JPH::ObjectLayer inLayer1, JPH::BroadPhaseLayer inLayer2) {
    return inLayer1 & *reinterpret_cast<JPH::ObjectLayer*>(&inLayer2);
}

static bool AssertFailedImpl(const char *inExpression, const char *inMessage, const char *inFile, uint inLine) {
    AssertFailure(true, inExpression, inMessage, inFile, inLine, "JoltPhysics");
    return false;
}
}


void PhysicsWorld::Start() {
    using namespace JPH;

    // Create internals
    internal_ = new Jolt::ComponentInternal;

    // This is the max amount of rigid bodies that you can add to the physics system. If you try to add more you'll get an error.
    constexpr uint cMaxBodies = 65536;

    // This determines how many mutexes to allocate to protect rigid bodies from concurrent access. Set it to 0 for the default settings.
    constexpr uint cNumBodyMutexes = 0;

    // This is the max amount of body pairs that can be queued at any time (the broad phase will detect overlapping
    // body pairs based on their bounding boxes and will insert them into a queue for the narrowphase). If you make this buffer
    // too small the queue will fill up and the broad phase jobs will start to do narrow phase work. This is slightly less efficient.
    constexpr uint cMaxBodyPairs = 65536;

    // This is the maximum size of the contact constraint buffer. If more contacts (collisions between bodies) are detected than this
    // number then these contacts will be ignored and bodies will start interpenetrating / fall through the world.
    constexpr uint cMaxContactConstraints = 10240;

    // Now we can initialize the actual physics system.
    internal_->physicsSystem_.Init(cMaxBodies, cNumBodyMutexes, cMaxBodyPairs, cMaxContactConstraints, internal_->broadPhaseLayerInterface_, Jolt::MyBroadPhaseCanCollide, Jolt::MyObjectCanCollide);

    // Optional step: Before starting the physics simulation you can optimize the broad phase. This improves collision detection performance.
    // You should definitely not call this every frame or when e.g. streaming in a new level section as it is an expensive operation.
    // Instead insert all new objects in batches instead of 1 at a time to keep the broad phase efficient.
    internal_->physicsSystem_.OptimizeBroadPhase();
}
void PhysicsWorld::Stop() {
    delete internal_;
}

void PhysicsWorld::RegisterObject(Context *context) {
    context->AddFactoryReflection<RigidBody>(Category_Physics);

    {
        using namespace JPH;

        // Register allocation hook
        RegisterDefaultAllocator();

        // Install callbacks
        JPH_IF_ENABLE_ASSERTS(AssertFailed = Jolt::AssertFailedImpl;)

        // Create a factory
        Factory::sInstance = new Factory();

        // Register all Jolt physics types
        RegisterTypes();
    }
}

void PhysicsWorld::Update(float timeDelta) {
    // If you take larger steps than 1 / 60th of a second you need to do multiple collision steps in order to keep the simulation stable. Do 1 collision step per 1 / 60th of a second (round up).
    constexpr int cCollisionSteps = 1;

    // If you want more accurate step results you can do multiple sub steps within a collision step. Usually you would set this to 1.
    constexpr int cIntegrationSubSteps = 1;

    // Step the world
    internal_->physicsSystem_.Update(timeDelta, cCollisionSteps, cIntegrationSubSteps, &internal_->tempAllocator_, &internal_->jobSystem_);
}

JPH::Body* PhysicsWorld::CreateBody(const JPH::BodyCreationSettings& creationSettings) {
    return internal_->physicsSystem_.GetBodyInterface().CreateBody(creationSettings);
}
void PhysicsWorld::ReleaseBody(JPH::Body* body) {
    internal_->physicsSystem_.GetBodyInterface().RemoveBody(body->GetID());
}

void PhysicsWorld::AddBody(JPH::Body* body, bool updateNow) {
    internal_->physicsSystem_.GetBodyInterface().AddBody(body->GetID(), updateNow?JPH::EActivation::Activate:JPH::EActivation::DontActivate);
}
void PhysicsWorld::RemoveBody(JPH::Body* body) {
    internal_->physicsSystem_.GetBodyInterface().RemoveBody(body->GetID());
}
bool PhysicsWorld::IsBodyAdded(JPH::Body* body) {
    return internal_->physicsSystem_.GetBodyInterface().IsAdded(body->GetID());
}
}
