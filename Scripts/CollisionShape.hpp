#ifndef _COLLISIONSHAPE_HPP
#define _COLLISIONSHAPE_HPP

#include "LevelManager.hpp"

#include <Urho3D/Scene/Component.h>
#include <Urho3D/Math/BoundingBox.h>



namespace JPH {
template<class T>
class Ref;
class Shape;
class ShapeSettings;
class CompoundShapeSettings;
}
namespace Game {
class RigidBody;

/// Collision shape type.
enum ShapeType
{
    SHAPE_BOX = 0,
    SHAPE_CYLINDER,
    SHAPE_SPHERE
};

class CollisionShape : public Component
{
    URHO3D_OBJECT(CollisionShape, Component)

    struct Internal;

public:
    /// Construct.
    explicit CollisionShape(Context* context);
    /// Destruct. Free the geometry data and clean up unused data from the geometry data cache.
    ~CollisionShape() override;
    /// Register object factory.
    /// @nobind
    static void RegisterObject(Context* context);

    /// Apply attribute changes that can not be applied immediately. Called after scene load or a network update.
    void ApplyAttributes() override;
    /// Handle enabled/disabled state change.
    void OnSetEnabled() override;

    /// Set as a box.
    void SetBox(const Vector3& size, const Vector3& position = Vector3::ZERO, const Quaternion& rotation = Quaternion::IDENTITY);
    /// Set as a sphere.
    void SetSphere(float diameter, const Vector3& position = Vector3::ZERO, const Quaternion& rotation = Quaternion::IDENTITY);
    /// Set as a cylinder.
    void SetCylinder(float diameter, float height, const Vector3& position = Vector3::ZERO, const Quaternion& rotation = Quaternion::IDENTITY);

    /// Set shape type.
    /// @property
    void SetShapeType(ShapeType type);
    /// Set shape size.
    /// @property
    void SetSize(const Vector3& size);
    /// Set offset position.
    /// @property
    void SetPosition(const Vector3& position);
    /// Set offset rotation.
    /// @property
    void SetRotation(const Quaternion& rotation);
    /// Set offset transform.
    void SetTransform(const Vector3& position, const Quaternion& rotation);

    /// Return Jolt collision shape settings.
    const JPH::ShapeSettings& GetCollisionShapeSettings() const;

    /// Return shape type.
    /// @property
    ShapeType GetShapeType() const { return shapeType_; }

    /// Return shape size.
    /// @property
    const Vector3& GetSize() const { return size_; }

    /// Return offset position.
    /// @property
    const Vector3& GetPosition() const { return position_; }

    /// Return offset rotation.
    /// @property
    const Quaternion& GetRotation() const { return rotation_; }

    /// Return world-space bounding box.
    /// @property
    BoundingBox GetWorldBoundingBox() const;

    /// Update the new collision shape to the RigidBody.
    void UpdateRigidBody();
    /// Create collision shape
    JPH::Ref<JPH::Shape> CreateShape() const;

private:
    /// Set as a box inernally.
    void InternalSetBox(const Vector3& size);
    /// Set as a sphere inernally.
    void InternalSetSphere(float radius);
    /// Set as a cylinder inernally.
    void InternalSetCylinder(float radius, float halfHeight);

    /// Update the collision shape after attribute changes.
    void UpdateShape();
    /// Mark shape dirty.
    void MarkShapeDirty() { recreateShape_ = true; }

    /// Rigid body.
    WeakPtr<RigidBody> rigidBody_;
    /// Collision shape type.
    ShapeType shapeType_;
    /// Offset position.
    Vector3 position_;
    /// Offset rotation.
    Quaternion rotation_;
    /// Shape size.
    Vector3 size_;
    // Internal Jolt structures.
    Internal *internal_;
    /// Recreate collision shape flag.
    mutable bool recreateShape_;
};
}
#endif // _COLLISIONSHAPE_HPP
