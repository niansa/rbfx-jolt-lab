#include "RigidBody.hpp"
#include "CollisionShape.hpp"
#include "PhysicsWorld.hpp"

#include <Urho3D/Core/CoreEvents.h>

#include <Jolt/Jolt.h>
#include <Jolt/Physics/Body/Body.h>
#include <Jolt/Physics/Body/BodyCreationSettings.h>
#include <Jolt/Physics/Collision/Shape/StaticCompoundShape.h>


namespace Game {
static const float DEFAULT_MASS = 0.0f;
static const float DEFAULT_FRICTION = 0.5f;
static const float DEFAULT_RESTITUTION = 0.0f;
static const float DEFAULT_ROLLING_FRICTION = 0.0f;
static const unsigned DEFAULT_COLLISION_LAYER = 0x1;
static const unsigned DEFAULT_COLLISION_MASK = M_MAX_UNSIGNED;

static const char* collisionEventModeNames[] =
{
    "Never",
    "When Active",
    "Always",
    nullptr
};

class RigidBody::Internal
{
    JPH::Body* body_ = nullptr;
    bool bodyIsInWorld_ = false;

public:
    JPH::BodyCreationSettings bodyCreationSettings_;
    JPH::StaticCompoundShapeSettings compoundShapeSettings_;
    ea::vector<JPH::Ref<JPH::Shape>> shapes_;
    WeakPtr<PhysicsWorld> world_;

    Internal() {}
    ~Internal() {
        SetBody(nullptr);
    }

    void SetBody(JPH::Body *newBody) {
        if (bodyIsInWorld_) {
            if (body_) {
                world_->RemoveBody(body_);
                world_->ReleaseBody(body_);
            }
            body_ = newBody;
            if (body_)
                world_->AddBody(body_, true);
        } else {
            if (body_)
                world_->ReleaseBody(body_);
            body_ = newBody;
        }
    }
    void ResetBody() {
        if (body_) {
            if (bodyIsInWorld_)
                world_->RemoveBody(body_);
            world_->ReleaseBody(body_);
            body_ = nullptr;
        }
    }
    void EnableBody() {
        bodyIsInWorld_ = true;
        if (body_)
            world_->AddBody(body_, true);
    }
    void DisableBody() {
        bodyIsInWorld_ = false;
        if (body_)
            world_->RemoveBody(body_);
    }
    JPH::Body *GetBody() const {
        return body_;
    }
};

template<typename T>
static bool IfNonEqualSetAndReturnTrue(T a, T b) {
    if (a != b) {
        a = b;
        return true;
    }
    return false;
}

RigidBody::RigidBody(Context* context) :
    Component(context),
    gravityOverride_(Vector3::ZERO),
    centerOfMass_(Vector3::ZERO),
    autoMass_(true),
    mass_(DEFAULT_MASS),
    collisionLayer_(DEFAULT_COLLISION_LAYER),
    collisionMask_(DEFAULT_COLLISION_MASK),
    collisionEventMode_(COLLISION_ACTIVE),
    lastPosition_(Vector3::ZERO),
    lastRotation_(Quaternion::IDENTITY),
    bodyDirty_(false),
    enableBodyUpdate_(true),
    hasSimulated_(false)
{
    internal_ = new Internal;
}

RigidBody::~RigidBody()
{
    delete internal_;
}

void RigidBody::RegisterObject(Context* context)
{
    context->AddFactoryReflection<RigidBody>(Category_Physics);

    URHO3D_ACCESSOR_ATTRIBUTE("Is Enabled", IsEnabled, SetEnabled, bool, true, AM_DEFAULT);
    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Physics Rotation", GetRotation, SetRotation, Quaternion, Quaternion::IDENTITY, AM_FILE | AM_NOEDIT);
    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Physics Position", GetPosition, SetPosition, Vector3, Vector3::ZERO, AM_FILE | AM_NOEDIT);
    URHO3D_ATTRIBUTE_EX("Auto Mass", bool, autoMass_, MarkBodyDirty, true, AM_DEFAULT);
    URHO3D_ATTRIBUTE_EX("Mass", float, mass_, MarkBodyDirty, DEFAULT_MASS, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Friction", GetFriction, SetFriction, float, DEFAULT_FRICTION, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Restitution", GetRestitution, SetRestitution, float, DEFAULT_RESTITUTION, AM_DEFAULT);
    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Linear Velocity", GetLinearVelocity, SetLinearVelocity, Vector3, Vector3::ZERO,
        AM_DEFAULT);
    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Angular Velocity", GetAngularVelocity, SetAngularVelocity, Vector3, Vector3::ZERO, AM_FILE);
    URHO3D_ACCESSOR_ATTRIBUTE("Linear Damping", GetLinearDamping, SetLinearDamping, float, 0.0f, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Angular Damping", GetAngularDamping, SetAngularDamping, float, 0.0f, AM_DEFAULT);
    URHO3D_ATTRIBUTE_EX("Collision Layer", int, collisionLayer_, MarkBodyDirty, DEFAULT_COLLISION_LAYER, AM_DEFAULT);
    URHO3D_ATTRIBUTE_EX("Collision Mask", int, collisionMask_, MarkBodyDirty, DEFAULT_COLLISION_MASK, AM_DEFAULT);
    URHO3D_ENUM_ATTRIBUTE_EX("Collision Event Mode", collisionEventMode_, MarkBodyDirty, collisionEventModeNames, COLLISION_ACTIVE, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Use Gravity", GetUseGravity, SetUseGravity, bool, true, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Is Kinematic", IsKinematic, SetKinematic, bool, false, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Is Trigger", IsTrigger, SetTrigger, bool, false, AM_DEFAULT);
}

void RigidBody::ApplyAttributes()
{
    if (bodyDirty_)
        UpdateBody();
}

void RigidBody::OnSetEnabled()
{
    bool enabled = IsEnabledEffective();

    if (enabled)
        internal_->EnableBody();
    else
        internal_->DisableBody();
}

void RigidBody::SetAutoMass(bool autoMass)
{
    autoMass_ = autoMass;
}
void RigidBody::SetMass(float mass)
{
    mass = Max(mass, 0.0f);

    if (IfNonEqualSetAndReturnTrue(mass, mass_)) {
        UpdateBody();
    }
}

void RigidBody::SetPosition(const Vector3& position)
{
    if (internal_->GetBody())
    {
        internal_->bodyCreationSettings_.mPosition = JPH::Vec3(position.x_, position.y_, position.z_);
        UpdateBody();
    }
}

void RigidBody::SetRotation(const Quaternion& rotation)
{
    if (internal_->GetBody())
    {
        internal_->bodyCreationSettings_.mRotation = JPH::Quat(rotation.x_, rotation.y_, rotation.z_, rotation.w_);
        UpdateBody();
    }
}

void RigidBody::SetTransform(const Vector3& position, const Quaternion& rotation)
{
    if (internal_->GetBody())
    {
        internal_->bodyCreationSettings_.mPosition = JPH::Vec3(position.x_, position.y_, position.z_);
        internal_->bodyCreationSettings_.mRotation = JPH::Quat(rotation.x_, rotation.y_, rotation.z_, rotation.w_);
        UpdateBody();
    }
}

void RigidBody::SetLinearVelocity(const Vector3& velocity)
{
    JPH::Vec3 jphVelocity(velocity.x_, velocity.y_, velocity.z_);
    if (internal_->GetBody())
    {
        internal_->GetBody()->SetLinearVelocity(jphVelocity);
    } else {
        internal_->bodyCreationSettings_.mLinearVelocity = jphVelocity;
    }
}

void RigidBody::SetLinearDamping(float damping)
{
    if (IfNonEqualSetAndReturnTrue(internal_->bodyCreationSettings_.mLinearDamping, damping)) {
        UpdateBody();
    }
}

void RigidBody::SetAngularVelocity(const Vector3& velocity)
{
    JPH::Vec3 jphVelocity(velocity.x_, velocity.y_, velocity.z_);
    if (internal_->GetBody())
    {
        internal_->GetBody()->SetAngularVelocity(jphVelocity);
    } else {
        internal_->bodyCreationSettings_.mAngularVelocity = jphVelocity;
    }
}

void RigidBody::SetAngularDamping(float damping)
{
    if (IfNonEqualSetAndReturnTrue(internal_->bodyCreationSettings_.mAngularDamping, damping)) {
        UpdateBody();
    }
}

void RigidBody::SetFriction(float friction)
{
    if (internal_->GetBody())
    {
        internal_->GetBody()->SetFriction(friction);
    }
    internal_->bodyCreationSettings_.mFriction = friction;
}

void RigidBody::SetRestitution(float restitution)
{
    if (internal_->GetBody())
    {
        internal_->GetBody()->SetRestitution(restitution);
    }
    internal_->bodyCreationSettings_.mRestitution = restitution;
}

void RigidBody::SetUseGravity(bool enable)
{
    if (IfNonEqualSetAndReturnTrue(internal_->bodyCreationSettings_.mGravityFactor, enable?1.0f:0.0f)) {
        UpdateBody();
    }
}

void RigidBody::SetKinematic(bool enable)
{
    auto jphType = enable?JPH::EMotionType::Kinematic:JPH::EMotionType::Dynamic;
    if (internal_->GetBody())
    {
        internal_->GetBody()->SetMotionType(jphType);
    }
    internal_->bodyCreationSettings_.mMotionType = jphType;
}

void RigidBody::SetTrigger(bool enable)
{
    auto jphType = enable?JPH::EMotionType::Static:JPH::EMotionType::Dynamic;
    if (internal_->GetBody())
    {
        internal_->GetBody()->SetMotionType(jphType);
        internal_->GetBody()->SetIsSensor(enable);
    }
    internal_->bodyCreationSettings_.mMotionType = jphType;
    internal_->bodyCreationSettings_.mIsSensor = enable;
}

void RigidBody::ApplyForce(const Vector3& force)
{
    if (internal_->GetBody() && force != Vector3::ZERO)
    {
        internal_->GetBody()->AddForce(JPH::Vec3(force.x_, force.y_, force.z_));
    }
}

void RigidBody::ApplyForce(const Vector3& force, const Vector3& position)
{
    if (internal_->GetBody() && force != Vector3::ZERO)
    {
        internal_->GetBody()->AddForce(JPH::Vec3(force.x_, force.y_, force.z_), JPH::Vec3(position.x_, position.y_, position.z_));
    }
}

void RigidBody::ApplyTorque(const Vector3& torque)
{
    if (internal_->GetBody() && torque != Vector3::ZERO)
    {
        internal_->GetBody()->AddTorque(JPH::Vec3(torque.x_, torque.y_, torque.z_));
    }
}

void RigidBody::ApplyImpulse(const Vector3& impulse)
{
    if (internal_->GetBody() && impulse != Vector3::ZERO)
    {
        internal_->GetBody()->AddImpulse(JPH::Vec3(impulse.x_, impulse.y_, impulse.z_));
    }
}

void RigidBody::ApplyImpulse(const Vector3& impulse, const Vector3& position)
{
    if (internal_->GetBody() && impulse != Vector3::ZERO)
    {
        internal_->GetBody()->AddImpulse(JPH::Vec3(impulse.x_, impulse.y_, impulse.z_), JPH::Vec3(position.x_, position.y_, position.z_));
    }
}

void RigidBody::ApplyTorqueImpulse(const Vector3& torque)
{
    if (internal_->GetBody() && torque != Vector3::ZERO)
    {
        internal_->GetBody()->AddAngularImpulse(JPH::Vec3(torque.x_, torque.y_, torque.z_));
    }
}

void RigidBody::DisableBodyUpdate()
{
    enableBodyUpdate_ = false;
}

void RigidBody::EnableBodyUpdate()
{
    if (!enableBodyUpdate_)
    {
        enableBodyUpdate_ = true;
        UpdateBody();
    }
}

Vector3 RigidBody::GetPosition() const
{
    if (internal_->GetBody())
    {
        auto jphPos = internal_->GetBody()->GetPosition();
        return {jphPos.GetX(), jphPos.GetY(), jphPos.GetZ()};
    }
    else
        return GetNode()->GetWorldPosition();
}

Quaternion RigidBody::GetRotation() const
{
    if (internal_->GetBody())
    {
        auto jphRot = internal_->GetBody()->GetRotation();
        return {jphRot.GetX(), jphRot.GetY(), jphRot.GetZ(), jphRot.GetW()};
    }
    else
        return GetNode()->GetWorldRotation();
}

Vector3 RigidBody::GetLinearVelocity() const
{
    if (internal_->GetBody()) {
        auto jphVel = internal_->GetBody()->GetLinearVelocity();
        return {jphVel.GetX(), jphVel.GetY(), jphVel.GetZ()};
    } else
        return Vector3::ZERO;
}

Vector3 RigidBody::GetVelocityAtPoint(const Vector3& position) const
{
    if (internal_->GetBody()) {
        auto jphVel = internal_->GetBody()->GetPointVelocity(JPH::Vec3(position.x_, position.y_, position.z_));
        return {jphVel.GetX(), jphVel.GetY(), jphVel.GetZ()};
    } else
        return Vector3::ZERO;
}

float RigidBody::GetLinearDamping() const
{
    return internal_->bodyCreationSettings_.mLinearDamping;
}

Vector3 RigidBody::GetAngularVelocity() const
{
    if (internal_->GetBody()) {
        auto jphVel = internal_->GetBody()->GetAngularVelocity();
        return {jphVel.GetX(), jphVel.GetY(), jphVel.GetZ()};
    } else
        return Vector3::ZERO;
}

float RigidBody::GetAngularDamping() const
{
    return internal_->bodyCreationSettings_.mAngularDamping;
}

float RigidBody::GetFriction() const
{
    return internal_->bodyCreationSettings_.mFriction;
}

float RigidBody::GetRestitution() const
{
    return internal_->bodyCreationSettings_.mRestitution;
}

bool RigidBody::GetUseGravity() const {
    return internal_->bodyCreationSettings_.mGravityFactor != 0.0f;
}

Vector3 RigidBody::GetCenterOfMass() const {
    if (internal_->GetBody())
    {
        auto jphPos = internal_->GetBody()->GetCenterOfMassPosition();
        return {jphPos.GetX(), jphPos.GetY(), jphPos.GetZ()};
    } else
        return Vector3::ZERO;
}

bool RigidBody::IsKinematic() const {
    return internal_->bodyCreationSettings_.mMotionType == JPH::EMotionType::Kinematic;
}

bool RigidBody::IsTrigger() const {
    return internal_->bodyCreationSettings_.mIsSensor;
}

bool RigidBody::IsActive() const
{
    return internal_->GetBody() ? internal_->GetBody()->IsActive() : false;
}

PhysicsWorld* RigidBody::GetPhysicsWorld() const
{
    return internal_->world_;
}

JPH::Body* RigidBody::GetBody() const {
    return internal_->GetBody();
}

float RigidBody::GetMass() const {
    return internal_->bodyCreationSettings_.mMassPropertiesOverride.mMass;
}

void RigidBody::UpdateBody()
{
    if (!GetPhysicsWorld()) {
        bodyDirty_ = true;
        return;
    }

    if (internal_->shapes_.empty()) {
        internal_->SetBody(nullptr);
        return;
    }

    if (!enableBodyUpdate_)
        return;

    internal_->bodyCreationSettings_.SetShapeSettings(&internal_->compoundShapeSettings_);

    auto massProps = internal_->bodyCreationSettings_.GetMassProperties();
    if (autoMass_) {
        massProps.mMass = mass_;
        internal_->bodyCreationSettings_.mOverrideMassProperties = JPH::EOverrideMassProperties::CalculateMassAndInertia;
    } else {
        if (mass_ > 0.0f) {
            massProps.mMass = mass_;
        } else {
            // Jolt doesn't allow zero mass
            massProps.mMass = 1.0f;
            internal_->bodyCreationSettings_.mMotionType = JPH::EMotionType::Static;
        }
        internal_->bodyCreationSettings_.mOverrideMassProperties = JPH::EOverrideMassProperties::CalculateInertia;
    }
    internal_->bodyCreationSettings_.mMassPropertiesOverride = massProps;

    internal_->bodyCreationSettings_.mRotation = internal_->bodyCreationSettings_.mRotation.Normalized();
    internal_->SetBody(GetPhysicsWorld()->CreateBody(internal_->bodyCreationSettings_));
}

void RigidBody::UpdateColliders() {
    ea::vector<CollisionShape*> shapeComponents;
    GetNode()->GetComponents<CollisionShape>(shapeComponents);
    internal_->compoundShapeSettings_.mSubShapes.clear();
    internal_->shapes_.clear();
    for (auto shapeComponent : shapeComponents) {
        auto shape = shapeComponent->CreateShape();
        if (!shape)
             continue;
        auto position = shapeComponent->GetPosition() + GetNode()->GetWorldPosition();
        auto rotation = shapeComponent->GetRotation() + GetNode()->GetWorldRotation();
        rotation.Normalize();
        internal_->compoundShapeSettings_.AddShape(JPH::Vec3(position.x_, position.y_, position.z_), JPH::Quat(rotation.x_, rotation.y_, rotation.z_, rotation.w_), shape);
        internal_->shapes_.push_back(ea::move(shape));
    }

    UpdateBody();
}

void RigidBody::OnMarkedDirty(Node* node)
{
    enableBodyUpdate_ = false;

    bool toUpdate = false;

    // If node transform changes, apply it back to the physics transform.
    if (!IsKinematic() || !hasSimulated_)
    {
        // Check if transform has changed from the last one set in ApplyWorldTransform()
        Vector3 newPosition = node_->GetWorldPosition();
        Quaternion newRotation = node_->GetWorldRotation();

        if (!newRotation.Equals(lastRotation_))
        {
            toUpdate = true;
            lastRotation_ = newRotation;
            SetRotation(newRotation);
        }
        if (!newPosition.Equals(lastPosition_))
        {
            toUpdate = true;
            lastPosition_ = newPosition;
            SetPosition(newPosition);
        }
    }

    enableBodyUpdate_ = true;

    if (toUpdate) {
        UpdateBody();
    }
}

void RigidBody::OnUpdate(StringHash, VariantMap&) {
    GetNode()->SetWorldPosition(GetPosition());
    GetNode()->SetWorldRotation(GetRotation());

    auto pos = GetPosition();
    printf("New position of %p: x=%f y=%f z=%f\n", this, pos.x_, pos.y_, pos.z_);
}

void RigidBody::OnNodeSet(Node* previousNode, Node* currentNode)
{
    if (previousNode)
        previousNode->RemoveListener(this);

    if (currentNode)
        currentNode->AddListener(this);

    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(RigidBody, OnUpdate));
}

void RigidBody::OnSceneSet(Scene* scene)
{
    if (scene)
    {
        if (scene == node_)
            URHO3D_LOGWARNING("RigidBody should not be created directly in the root scene node");

        internal_->world_ = scene->GetOrCreateComponent<PhysicsWorld>();

        UpdateColliders();

        internal_->EnableBody();
    }
    else
    {
        internal_->DisableBody();
    }
}
}
