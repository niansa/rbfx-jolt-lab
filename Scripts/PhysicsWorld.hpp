#ifndef _PHYSICSWORLD_HPP
#define _PHYSICSWORLD_HPP

#include "LevelManager.hpp"

#include <Urho3D/Scene/LogicComponent.h>



namespace JPH {
class BodyCreationSettings;
class Body;
}
namespace Game {
namespace Jolt {
struct ComponentInternal;
}

class RigidBody;

class PhysicsWorld : public LogicComponent {
    URHO3D_OBJECT(PhysicsWorld, LogicComponent)

public:
    /// Construct.
    explicit PhysicsWorld(Context* ctx) : LogicComponent(ctx) {}
    /// Register object factory.
    /// @nobind
    static void RegisterObject(Context* context);


    void Start() override;
    void Stop() override;
    void Update(float timeDelta) override;

    /// Create body. It won't be kept track of yet. Called by RigidBody.
    JPH::Body *CreateBody(const JPH::BodyCreationSettings&);
    /// Remove body. Called by RigidBody.
    void ReleaseBody(JPH::Body* body);
    /// Enable a body to keep track of. Called by RigidBody.
    void AddBody(JPH::Body*, bool updateNow);
    /// Disable a body. Called by RigidBody.
    void RemoveBody(JPH::Body*);
    /// Check if rigid body is enabled. Called by RigidBody.
    bool IsBodyAdded(JPH::Body*);

    /// Create and add body. Called by RigidBody.
    JPH::Body *CreateAndAddBody(const JPH::BodyCreationSettings& settings, bool updateNow) {
        auto body = CreateBody(settings);
        AddBody(body, updateNow);
        return body;
    }
    /// Remove and release body. Called by RigidBody.
    void RemoveAndReleaseBody(JPH::Body* body) {
        if (IsBodyAdded(body))
            RemoveBody(body);
        ReleaseBody(body);
    }

protected:
    /// Internal data structures.
    Jolt::ComponentInternal *internal_;
};
}
#endif // _PHYSICSWORLD_HPP
