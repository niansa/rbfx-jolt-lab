#pragma once

namespace Game {
class Player;
}
#ifndef PLAYER_HPP
#define PLAYER_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Input/Input.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Scene/LogicComponent.h>



namespace Game {

class Player final : public LogicComponent {
    URHO3D_OBJECT(Player, LogicComponent);

public:
    using LogicComponent::LogicComponent;

    void Start() override;
};
}
#endif // PLAYER_HPP
