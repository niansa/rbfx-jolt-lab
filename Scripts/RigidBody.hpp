#ifndef _RIGIDBODY_HPP
#define _RIGIDBODY_HPP

#include "LevelManager.hpp"

#include <Urho3D/Scene/Component.h>



namespace JPH {
class Body;
class CompoundShapeSettings;
}
namespace Game {
class PhysicsWorld;
class CollisionShape;

/// Rigid body collision event signaling mode.
enum CollisionEventMode
{
    COLLISION_NEVER = 0,
    COLLISION_ACTIVE,
    COLLISION_ALWAYS
};

/// Physics rigid body component.
class RigidBody : public Component {
    URHO3D_OBJECT(RigidBody, Component)

    struct Internal;

public:
    /// Construct.
    explicit RigidBody(Context* context);
    /// Destruct. Free the rigid body and geometries.
    ~RigidBody() override;
    /// Register object factory.
    /// @nobind
    static void RegisterObject(Context* context);

    /// Apply attribute changes that can not be applied immediately. Called after scene load or a network update.
    void ApplyAttributes() override;
    /// Handle enabled/disabled state change.
    void OnSetEnabled() override;

    /// Specifies weather to automatically determine mass
    /// @property
    void SetAutoMass(bool autoMass);
    /// Set mass. Zero mass makes the body static.
    /// @property
    void SetMass(float mass);
    /// Set rigid body position in world space.
    /// @property
    void SetPosition(const Vector3& position);
    /// Set rigid body rotation in world space.
    /// @property
    void SetRotation(const Quaternion& rotation);
    /// Set rigid body position and rotation in world space as an atomic operation.
    void SetTransform(const Vector3& position, const Quaternion& rotation);
    /// Set linear velocity.
    /// @property
    void SetLinearVelocity(const Vector3& velocity);
    /// Set linear velocity damping factor.
    /// @property
    void SetLinearDamping(float damping);
    /// Set angular velocity.
    /// @property
    void SetAngularVelocity(const Vector3& velocity);
    /// Set angular velocity damping factor.
    /// @property
    void SetAngularDamping(float damping);
    /// Set friction coefficient.
    /// @property
    void SetFriction(float friction);
    /// Set restitution coefficient.
    /// @property
    void SetRestitution(float restitution);
    /// Set whether gravity is applied to rigid body.
    /// @property
    void SetUseGravity(bool enable);
    /// Set rigid body kinematic mode. In kinematic mode forces are not applied to the rigid body.
    /// @property
    void SetKinematic(bool enable);
    /// Set rigid body trigger mode. In trigger mode collisions are reported but do not apply forces.
    /// @property
    void SetTrigger(bool enable);
    /// Set collision layer.
    /// @property
    void SetCollisionLayer(unsigned layer);
    /// Set collision mask.
    /// @property
    void SetCollisionMask(unsigned mask);
    /// Set collision group and mask.
    void SetCollisionLayerAndMask(unsigned layer, unsigned mask);
    /// Set collision event signaling mode. Default is to signal when rigid bodies are active.
    /// @property
    void SetCollisionEventMode(CollisionEventMode mode);
    /// Apply force to center of mass.
    void ApplyForce(const Vector3& force);
    /// Apply force at local position.
    void ApplyForce(const Vector3& force, const Vector3& position);
    /// Apply torque.
    void ApplyTorque(const Vector3& torque);
    /// Apply impulse to center of mass.
    void ApplyImpulse(const Vector3& impulse);
    /// Apply impulse at local position.
    void ApplyImpulse(const Vector3& impulse, const Vector3& position);
    /// Apply torque impulse.
    void ApplyTorqueImpulse(const Vector3& torque);
    /// Disable body update. Call this to optimize performance when adding or editing multiple collision shapes in the same node.
    void DisableBodyUpdate();
    /// Re-enable body update and recalculate the mass/inertia by calling UpdateMass(). Call when collision shape changes are finished.
    void EnableBodyUpdate();

    /// Return physics world.
    PhysicsWorld* GetPhysicsWorld() const;

    /// Return Bullet rigid body.
    JPH::Body* GetBody() const;

    /// Return mass.
    /// @property
    float GetMass() const;

    /// Return rigid body position in world space.
    /// @property
    Vector3 GetPosition() const;
    /// Return rigid body rotation in world space.
    /// @property
    Quaternion GetRotation() const;
    /// Return linear velocity.
    /// @property
    Vector3 GetLinearVelocity() const;
    /// Return linear velocity at local point.
    Vector3 GetVelocityAtPoint(const Vector3& position) const;
    /// Return linear velocity damping factor.
    /// @property
    float GetLinearDamping() const;
    /// Return angular velocity.
    /// @property
    Vector3 GetAngularVelocity() const;
    /// Return angular velocity damping factor.
    /// @property
    float GetAngularDamping() const;
    /// Return friction coefficient.
    /// @property
    float GetFriction() const;
    /// Return restitution coefficient.
    /// @property
    float GetRestitution() const;

    /// Return whether rigid body uses gravity.
    /// @property
    bool GetUseGravity() const;

    /// Return center of mass offset.
    /// @property
    Vector3 GetCenterOfMass() const;

    /// Return kinematic mode flag.
    /// @property
    bool IsKinematic() const;

    /// Return whether this RigidBody is acting as a trigger.
    /// @property
    bool IsTrigger() const;

    /// Return whether rigid body is active (not sleeping).
    /// @property
    bool IsActive() const;

    /// Return collision layer.
    /// @property
    unsigned GetCollisionLayer() const { return collisionLayer_; }

    /// Return collision mask.
    /// @property
    unsigned GetCollisionMask() const { return collisionMask_; }

    /// Update colliders. Called by CollisionShape.
    void UpdateColliders();

    /// Update the rigid body. Will release the old body and create a new one.
    void UpdateBody();

protected:
    /// Handle node being assigned.
    void OnNodeSet(Node* previousNode, Node* currentNode) override;
    /// Handle scene being assigned.
    void OnSceneSet(Scene* scene) override;
    /// Handle node transform being dirtied.
    void OnMarkedDirty(Node* node) override;
    /// Handle next frame
    void OnUpdate(StringHash, VariantMap&);

private:
    /// Mark body dirty.
    void MarkBodyDirty() { bodyDirty_ = true; }

    /// Internal Jolt structures.
    Internal *internal_;
    /// Gravity override vector.
    Vector3 gravityOverride_;
    /// Center of mass offset.
    Vector3 centerOfMass_;
    /// Mass.
    bool autoMass_;
    float mass_;
    /// Collision layer.
    unsigned collisionLayer_;
    /// Collision mask.
    unsigned collisionMask_;
    /// Collision event signaling mode.
    CollisionEventMode collisionEventMode_;
    /// Last interpolated position from the simulation.
    mutable Vector3 lastPosition_;
    /// Last interpolated rotation from the simulation.
    mutable Quaternion lastRotation_;
    /// Body update enable flag.
    bool enableBodyUpdate_;
    /// Body dirty flag.
    bool bodyDirty_;
    /// Internal flag whether has simulated at least once.
    mutable bool hasSimulated_;
};
}
#endif // _RIGIDBODY_HPP
