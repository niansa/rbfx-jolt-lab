#include "Player.hpp"

#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Camera.h>



namespace Game {
void Player::Start() {
    // Create components
    auto camera = GetNode()->CreateComponent<Camera>();
    GetNode()->CreateComponent("FreeFlyController");
    // Set viewport camera
    SharedPtr<Viewport> viewport(new Viewport(context_, GetScene(), camera));
    GetSubsystem<Renderer>()->SetViewport(0, viewport);
}
}
