#ifndef _PHYICSWORLDINTERNALS_HPP
#define _PHYICSWORLDINTERNALS_HPP

#include <Jolt/Jolt.h>
#include <Jolt/Core/TempAllocator.h>
#include <Jolt/Core/JobSystemThreadPool.h>
#include <Jolt/Physics/PhysicsSystem.h>
#include <Jolt/Physics/Collision/BroadPhase/BroadPhase.h>




namespace Game {
class PhysicsWorld;

namespace Jolt {
enum {
    LAYER_NON_MOVING,
    LAYER_MOVING,
    NUM_LAYERS
};

// BroadPhaseLayerInterface implementation
// This defines a mapping between object and broadphase layers.
class BPLayerInterfaceImpl final : public JPH::BroadPhaseLayerInterface {
public:
    virtual uint GetNumBroadPhaseLayers() const override {
        return 32;
    }

    virtual JPH::BroadPhaseLayer GetBroadPhaseLayer(JPH::ObjectLayer inLayer) const override {
        return static_cast<JPH::BroadPhaseLayer>(inLayer);
    }

#if defined(JPH_EXTERNAL_PROFILE) || defined(JPH_PROFILE_ENABLED)
    virtual const char *GetBroadPhaseLayerName(JPH::BroadPhaseLayer inLayer) const override {
        // HACKY but works
        static std::string strCache_;
        return (strCache_ = std::to_string(JPH::BroadPhaseLayer::Type(inLayer))).c_str();
    }
#endif // JPH_EXTERNAL_PROFILE || JPH_PROFILE_ENABLED
};

class ComponentInternal {
    friend ::Game::PhysicsWorld;

    JPH::TempAllocatorImpl tempAllocator_;
    JPH::JobSystemThreadPool jobSystem_;
    BPLayerInterfaceImpl broadPhaseLayerInterface_;
public:
    JPH::PhysicsSystem physicsSystem_;

    ComponentInternal() :
        // We need a temp allocator for temporary allocations during the physics update. We're
        // pre-allocating 10 MB to avoid having to do allocations during the physics update.
        // B.t.w. 10 MB is way too much for this example but it is a typical value you can use.
        // If you don't want to pre-allocate you can also use TempAllocatorMalloc to fall back to
        // malloc / free.
        tempAllocator_(10 * 1024 * 1024),
        // We need a job system that will execute physics jobs on multiple threads. Typically
        // you would implement the JobSystem interface yourself and let Jolt Physics run on top
        // of your own job scheduler. JobSystemThreadPool is an example implementation.
        jobSystem_(JPH::cMaxPhysicsJobs, JPH::cMaxPhysicsBarriers, JPH::thread::hardware_concurrency() - 1),
        // Create mapping table from object layer to broadphase layer
        // Note: As this is an interface, PhysicsSystem will take a reference to this so this instance needs to stay alive!
        broadPhaseLayerInterface_()
    {}
};
}
}
#endif
